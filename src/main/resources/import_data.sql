drop table Products if exists;
CREATE TABLE PRODUCTS(PRODUCT_ID INT PRIMARY KEY,TITLE VARCHAR(255), DESCRIPTION VARCHAR(255), COST DOUBLE, INSERTIONDATE DATE);
INSERT INTO products (product_id, title, description, cost, insertiondate) VALUES (1,'Effective Java', 'by Jaushua Blocjh', 20,sysdate());