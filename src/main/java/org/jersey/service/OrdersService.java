package org.jersey.service;

import java.util.List;

import javax.ws.rs.core.Response;

import org.persistence.model.Order;
import org.persistence.model.Product;

public interface OrdersService {

	public Order getOrderById(int id);
	
	public List<Order> getOrders();
	
	public Response deleteOrderById(int id);

	public Response updateOrderById(int id, String address);

	public Response createOrder(int id, String address, int product_id);
}
