package org.jersey.service;

import java.util.List;

import org.persistence.model.Product;

public interface ProductsService {

	public Product getProductById(int id);
	
	public List<Product> getProducts();
}
