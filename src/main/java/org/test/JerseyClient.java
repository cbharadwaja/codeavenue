package org.test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;

public class JerseyClient {

	public static void main(String[] args) {
		
		Client client = ClientBuilder.newClient();
		 
		WebTarget webResource = client
		   .target("http://localhost:8080/java-webapp-coding-test/products/get");
 
		Builder request = webResource.request();
		
		String response = request.get(String.class);
 
		System.out.println(response);
	}
}

