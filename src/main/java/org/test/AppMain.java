package org.test;

import java.util.List;

import org.persistence.dao.OrderDao;
import org.persistence.dao.ProductDao;
import org.persistence.model.Order;
import org.persistence.model.Product;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AppMain {	
	
	public static void main(String[] args) {
		ApplicationContext appContext = 
	    		new ClassPathXmlApplicationContext("applicationContext.xml");
		
		ProductDao productdao = (ProductDao) appContext.getBean("productdao");
		OrderDao  orderdao = (OrderDao) appContext.getBean("orderdao");
//		productdao.addProduct(3, "Effective Java", "By Josh Bloch", 20.0, new Date());
		
		List<Product> products = productdao.getProducts();
		System.out.println(products.size());
		for (Product product : products) {
			System.out.println(product.toString());
		}
		
		Product product = productdao.getProductById(1);
		System.out.println(product.toString());
		
		orderdao.createOrder(2, "Linden Arms", product);
		
		Order order = orderdao.getOrderById(2);
		System.out.println(order.toString());
		
		orderdao.updateOrder(2, "Normandie Blvd");
		
		List<Order> orders = orderdao.getOrders();
		for (Order order1 : orders) {
			System.out.println(order1.toString());
		}
		
		((AbstractApplicationContext) appContext).close();
		
		System.out.println("Done");
		
	}

}
