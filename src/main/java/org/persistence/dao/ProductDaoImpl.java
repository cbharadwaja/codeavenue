package org.persistence.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.persistence.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public class ProductDaoImpl implements ProductDao{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Product> getProducts() {
		return getSession().createQuery("from " +Product.class.getName()).list();
	}

	@Override
	public Product getProductById(int id) {
		return (Product) getSession().get(Product.class.getName(), id);
	}

	/*public boolean addProduct(int id, String title, String description, double cost, Date insertionDate) {
		Product product = new Product(id, title, description, cost, insertionDate);
		getSession().save(product);
		return true;
	}*/
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	protected final Session getSession() {
		return sessionFactory.getCurrentSession();
	}
}
