package org.persistence.dao;

import java.util.List;

import org.persistence.model.Order;
import org.persistence.model.Product;

public interface OrderDao {

	public List<Order> getOrders();
	
	public Order getOrderById(int id);
	
	public int deleteOrderById(int id);

	public boolean updateOrder(int id, String address);

	public int createOrder(int id, String address, Product product);
}
