package org.persistence.dao;

import java.util.List;

import org.persistence.model.Product;

public interface ProductDao {

	public List<Product> getProducts();
	
	public Product getProductById(int i);
}
