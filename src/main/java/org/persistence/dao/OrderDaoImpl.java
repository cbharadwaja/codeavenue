package org.persistence.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.persistence.model.Order;
import org.persistence.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Component
public class OrderDaoImpl implements OrderDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Order> getOrders() {
		return getSession().createQuery("from " +Order.class.getName()).list();
	}

	@Override
	public Order getOrderById(int id) {
		return (Order) getSession().get(Order.class.getName(), id);
	}

	@Override
	public int deleteOrderById(int id) {
		Order order = (Order) getSession().get(Order.class.getName(), id);
		getSession().delete(order);
		return 1;
	}

	@Override
	public int createOrder(int id, String address, Product product) {
		Order order = new Order(id, address, product);
		getSession().save(order);
		return id;
	}

	@Override
	public boolean updateOrder(int id, String address) {
		Order order = (Order) getSession().get(Order.class.getName(), id);
		order.setAddress(address);
		getSession().update(order);
		return true;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	protected final Session getSession() {
		return sessionFactory.getCurrentSession();
	}
}
