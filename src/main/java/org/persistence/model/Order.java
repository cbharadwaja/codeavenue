package org.persistence.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Table(name = "orders")
@Entity
public class Order implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5000293511171872191L;

	public Order() {
		
	}
	
	public Order(int id, String address, Product product) {
		this.orderId = id;
		this.address = address;
		this.product = product;
	}

	@Id
	@Column(name="order_id")
	private int orderId;
	
	private String address;
	
	private Date insertionDate;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "product_id")
	private Product product;

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getInsertionDate() {
		return insertionDate;
	}

	public void setInsertionDate(Date insertionDate) {
		this.insertionDate = insertionDate;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	@Override
	public String toString(){
		return new StringBuffer(this.address).toString();
	}
}