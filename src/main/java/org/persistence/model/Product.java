package org.persistence.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name="products")
public class Product implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7026627067332296556L;
	
	@Id
	@Column(name="product_id")
	private int id;
	
	private String title;
	
	private String description;

	private double cost;
	
	private Date insertionDate;
	
	public Product(){}
	
	public Product(int id, String title, String description, double cost, Date date){ // NOPMD by Bharadwaja on 7/11/15 8:58 PM
		this.id = id;
		this.title = title;
		this.description = description;
		this.cost = cost;
		this.insertionDate = date;
	}
	
	public Product(String title, String descrption) {
		this.title = title;
		this.description = descrption;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}
	
	public Date getInsertionDate() {
		return insertionDate;
	}

	public void setInsertionDate(Date insertionDate) {
		this.insertionDate = insertionDate;
	}
	
	@Override
	public String toString(){
		return new StringBuffer(this.title).append(this.description).append(this.cost).toString();
	}
}
