package org.rest.service.impl;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jersey.service.ProductsService;
import org.persistence.dao.ProductDao;
import org.persistence.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Path("products")
public class ProductsServiceImpl implements ProductsService {

	@Autowired
	ProductDao productdao;
	
	@GET
	@Path("/{id}")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Transactional
	@Override
	public Product getProductById(@PathParam("id") int id) {
		return productdao.getProductById(id);
	}

	@GET
	@Path("/all")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Transactional
	@Override
	public List<Product> getProducts() {
		return productdao.getProducts();
	}

}
