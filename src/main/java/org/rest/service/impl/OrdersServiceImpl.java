package org.rest.service.impl;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jersey.service.OrdersService;
import org.persistence.dao.OrderDao;
import org.persistence.dao.ProductDao;
import org.persistence.model.Order;
import org.persistence.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Path("/orders")
public class OrdersServiceImpl implements OrdersService {

	@Autowired
	OrderDao orderdao;
	
	@Autowired
	ProductDao productdao;
	
	@GET
	@Path("/{id}")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Transactional
	@Override
	public Order getOrderById(@PathParam("id") int id) {
		return orderdao.getOrderById(id);
	}

	@GET
	@Path("/all")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Transactional
	@Override
	public List<Order> getOrders() {
		return orderdao.getOrders();
	}

	@Override
	@POST
	@Path("/add")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.TEXT_HTML, MediaType.APPLICATION_JSON })
	@Transactional
	public Response createOrder(@FormParam("id") int id, @FormParam("address") String address, @FormParam("product_id") int product_id) {
		Product product = productdao.getProductById(product_id);
		orderdao.createOrder(id, address, product);
		return Response.status(201)
				.entity("A new order/resource has been created").build();
	}

	@Override
	@DELETE
	@Path("delete/{id}")
	@Produces({ MediaType.TEXT_HTML })
	@Transactional
	public Response deleteOrderById(@PathParam("id") int id) {
		if (orderdao.deleteOrderById(id) == 1) {
			return Response.status(204).build();
		} else {
			return Response
					.status(404)
					.entity("order with the id " + id
							+ " is not present in the database").build();
		}
	}
	
	@Override
	@Path("update/{id}/{address}")
	@PUT
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.TEXT_HTML })
	@Transactional
	public Response updateOrderById(@PathParam("id") int id, @PathParam("address") String address) {
		orderdao.updateOrder(id, address);
		return Response.status(200).entity("order has been updated").build();
	}
}
