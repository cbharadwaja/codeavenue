package org.test.rest;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.junit.Assert;
import org.junit.Test;
import org.persistence.model.Product;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

public class RestServiceTest {
	
	@Test
	public void testGetProducts() throws JsonGenerationException,
	JsonMappingException, IOException {

		ClientConfig clientConfig = new ClientConfig();
		clientConfig.register(JacksonJaxbJsonProvider.class);

		Client client = ClientBuilder.newClient(clientConfig);

		WebTarget webTarget = client
				.target("http://localhost:8080/java-webapp-coding-test/products/all");
		
		Builder request = webTarget.request();
		
		request.header("Content-type", MediaType.APPLICATION_JSON);
		
		Response response = request.get();
		Assert.assertTrue(response.getStatus() == 200);
		
		List<Product> products = response
				.readEntity(new GenericType<List<Product>>() {
				});
		
		ObjectMapper mapper = new ObjectMapper();
		System.out.print(mapper.writerWithDefaultPrettyPrinter()
				.writeValueAsString(products));
		
		Assert.assertTrue("At least one podcast is present",
				products.size() > 0);
	}
	

	@Test
	public void testGetProduct() throws JsonGenerationException,
	JsonMappingException, IOException {

		ClientConfig clientConfig = new ClientConfig();
		clientConfig.register(JacksonJaxbJsonProvider.class);

		Client client = ClientBuilder.newClient(clientConfig);

		WebTarget webTarget = client
				.target("http://localhost:8080/java-webapp-coding-test/products/1");
		
		Builder request = webTarget.request();
		
		request.header("Content-type", MediaType.APPLICATION_JSON);
		
		Response response = request.get();
		Assert.assertTrue(response.getStatus() == 200);
		
		Product product = response.readEntity(Product.class);
		
		ObjectMapper mapper = new ObjectMapper();
		System.out.print(mapper.writerWithDefaultPrettyPrinter()
				.writeValueAsString(product));
	}
}